//
//  generator.hpp
//  sorter
//
//  Created by Александр Щукин on 18.04.16.
//
//

#ifndef generator_hpp
#define generator_hpp

#include <iosfwd>

void generate(std::ostream& strm, size_t recCount);

#endif /* generator_hpp */

//
//  config.cpp
//  Project
//
//  Created by Александр Щукин on 20.04.16.
//
//

#include "config.hpp"
#include <boost/program_options.hpp>
#include <iostream>
#include <unordered_map>


namespace
{
    typedef std::unordered_map<std::string, Config::Field> Fields;
    Fields const& fields()
    {
        auto init = []()->Fields
        {
            Fields fields;
            fields["brand"] = Config::BRAND;
            fields["model"] = Config::MODEL;
            fields["year"] = Config::YEAR;
            fields["price"] = Config::PRICE;
            return fields;
        };
        static Fields fields = init();
        return fields;
    }
    
    typedef std::unordered_map<std::string, Config::Condition> Conditions;
    Conditions const& conditions()
    {
        auto init = []()->Conditions
        {
            Conditions conditions;
            conditions["=="] = Config::EQUAL;
            conditions[">"] = Config::MORE;
            conditions[">="] = Config::MORE_EQUAL;
            conditions["<"] = Config::LESS;
            conditions["<="] = Config::LESS_EQUAL;
            conditions["!="] = Config::NOT_EQUAL;
            return conditions;
        };
        static Conditions conditions = init();
        return conditions;
    }
    
    // Чтобы хранить значние фильтрации в параметрах используется класс boost::any
    // Из входных данных достаем строки, а по имени поля можем определить тип значения
    void valueToAnyCast(Config::Field field, boost::any& anyValue, std::string const& strValue)
    {
        switch (field)
        {
            case Config::BRAND:
                anyValue = strValue;
                break;
                
            case Config::YEAR:
                anyValue = boost::lexical_cast<size_t>(strValue);
                break;
                
            case Config::PRICE:
                anyValue = boost::lexical_cast<float>(strValue);
                break;
                
            case Config::MODEL:; // по этому полю не фильтруем
        }
    }
    
} // namespace



Config::SortItem::SortItem(Field field) :
    field(field),
    order(Config::ASC)
{}

Config::SortItem::SortItem(Field field, Order order) :
    field(field),
    order(order)
{}




Config::FilterItem::FilterItem(Field field, Condition condition, boost::any&& value) :
    _field(field),
    _condition(condition),
    _value(value)
{}

Config::Field Config::FilterItem::field() const
{
    return _field;
}

Config::Condition Config::FilterItem::condition() const
{
    return _condition;
}

boost::any const& Config::FilterItem::value() const
{
    return _value;
}



Config::Config() :
    _sorting(4) // в данных только 4 поля
{
    // флаговые переменные не инициализиуем,
    // значения будут заданы при разборе командной строки
    // для остальных достаточно их конструкторов по умолчанию
}

bool Config::isGenerateTestData() const
{
    return _isGenerateTestData;
}

bool Config::isShowCpu() const
{
    return _isShowCpu;
}

std::string const& Config::input() const
{
    return _input;
}

std::string const& Config::output() const
{
    return _output;
}

Config::Filters const& Config::filters() const
{
    return _filters;
}

Config::Sorting const& Config::sorting() const
{
    return _sorting;
}


bool Config::parseArgs(int argc, const char* argv[])
{
    namespace po = boost::program_options;
    
    po::options_description desc("Options");
    desc.add_options()
    ("help,H", "Print help messages")
    ("filter,F", po::value<std::vector<std::string>>()->multitoken()->composing(),
        "Apply filtering\n"
        "usage: -F \"<name> <filter> <value>\" [\"<name> <filter> <value>\"]...\n"
        "<name> = [brand | year | price]\n"
        "brand <filter> = '=='\n"
        "  \"brand == Kia\" \"brand == 'Kia motors'\"\n"
        "year <filter> = '==' | '>' | '>=' | '<' | '<=' | '!='\n"
        "price <filter> = '>' | '<'")
    ("sort,S", po::value<std::vector<std::string>>()->multitoken()->composing(),
        "Apply sorting\n"
        "usage: -S <name> [desc] [<name> [desc]] ...\n"
        "<name> = [brand | model | year | price]\n"
        "desc - sort descending")
    ("input,I", po::value<std::string>(&_input),
        "Input file. By default using standart input stream\n"
        "usage: -I <file name>")
    ("output,O", po::value<std::string>(&_output),
        "Output file. By default using standart output stream\n"
        "usage: -O <file name>")
    ("cpu,C", po::value<bool>(&_isShowCpu)->default_value(false)->zero_tokens(),
        "Report information about spent time to standart error stream")
    ("generate,G", po::value<bool>(&_isGenerateTestData)->default_value(false)->zero_tokens(),
        "Generate 500,000 test data record to standart output")
    ;
    
    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        
        if (vm.count("help"))
        {
            std::cout << "Basic Command Line Parameter App" << std::endl << desc << std::endl;
        } else {
            if (vm.count("sort") + vm.count("filter") == 0)
            {
                throw po::error("Must specify --sort or --filter");
            }
            
            po::notify(vm);
            
            if (vm.count("sort"))
                parseSortOptions(vm["sort"].as<std::vector<std::string> const&>());
            if (vm.count("filter"))
                parseFilterOptions(vm["filter"].as<std::vector<std::string> const&>());
            
            return true;
        }
    }
    catch (po::error const& e)
    {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        // TODO return ERROR_IN_COMMAND_LINE;
    }
    return false;
}

void Config::parseSortOptions(std::vector<std::string> const& sortOptions)
{
    Fields const& flds = fields();
    
    for (auto const& opt : sortOptions)
    {
        auto it = flds.find(opt);
        if (it != flds.end())
        {
            _sorting.push_back(SortItem(it->second));
        }
        else if (opt == "desc" && !_sorting.empty())
        {
            _sorting.replace(_sorting.end() - 1, SortItem(_sorting.back().field, Config::DESC));
        }
        else
            throw boost::program_options::error(std::string("undefined field name value in option --sort: ").append(opt));
    }
}

void Config::parseFilterOptions(std::vector<std::string> const& filterOptions)
{
    namespace po = boost::program_options;
    
    Conditions const& conds = conditions();
    Fields const& flds = fields();
    
    std::string fldStr;
    std::string cndStr;
    std::string valStr;
    
    for (auto const& opt : filterOptions)
    {
        std::stringstream ss(opt);
        ss >> fldStr >> cndStr;
        
        // TODO можно проверять валидность параметра серьезнее, например если
        // ошибочно задали значние как "x = y'z" или "x = y z"
        if (*opt.rbegin() == '\'')
        {
            std::string::size_type pos = opt.find('\''); // как минимум найдем последнюю
            if (opt.size() - pos < 2)
                throw po::error(std::string("unrecognized value in option --filter: ").append(opt));
            
            valStr = opt.substr(pos + 1, opt.size() - (pos + 2)); // одиночные кавычки обрезаем
        } else
            ss >> valStr;
        
        auto fldIt = flds.find(fldStr);
        if (fldIt == flds.end() || fldIt->second == Config::MODEL)
            throw po::error(std::string("undefined field name value in option --filter: ").append(opt));
        
        auto cndIt = conds.find(cndStr);
        if (cndIt == conds.end())
            throw po::error(std::string("undefined condition operator value in option --filter: ").append(opt));
        
        try
        {
            boost::any value;
            valueToAnyCast(fldIt->second, value, valStr); // конвертация в boost::any с правильным типом
            _filters.push_back(FilterItem(fldIt->second, cndIt->second, std::move(value)));
        }
        catch(boost::bad_lexical_cast const&)
        {
            throw po::error(std::string("value of an unknown type in option --filter: ").append(opt));
        }
    }
}



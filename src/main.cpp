//
//  main.cpp
//  sorter
//
//  Created by Александр Щукин on 14.04.16.
//  Copyright © 2016 Александр Щукин. All rights reserved.
//

#include <iostream>
#include <string>
#include <iterator>
#include "generator.hpp"
#include "filter.hpp"
#include "car.hpp"
#include "sorter.hpp"
#include "config.hpp"
#include "perf_counter.hpp"
#include <set>
#include <fstream>


namespace
{
    int const SUCCESS = 0;
    int const ERROR_COMMAND_LINE = 1;
    int const IO_ERROR = 2;
    
    
    // помошник освобождения ресурсов при выходе из области видимости
    struct ScopeExit
    {
        std::function<void()> onExitHandler;
        
        ~ScopeExit()
        {
            // подход немного отличается от того, что есть в boost,
            // в данном случае освобождение ресурсов может и не понадобится
            if (onExitHandler)
                onExitHandler();
        }
    };
} // namespace


int main(int argc, const char* argv[])
{
    Config config;
    
    if (!config.parseArgs(argc, argv))
        // либо ошибка разбора параметров, либо вызвали с параметром --help
        return ERROR_COMMAND_LINE;
  
    if (config.isGenerateTestData())
    {
        // вывод тестовых данных в стандартный поток вывода
        generate(std::cout, 500 * 1000);
        return SUCCESS;
    }

    // настройка сортировки
    for (auto const& it : config.sorting())
    {
        bool isAscending = it.order == Config::ASC; // порядок сортировки
        switch (it.field)
        {
            case Config::BRAND:
                SorterConfig::instance().addSortByBrand(isAscending);
                break;
            case Config::MODEL:
                SorterConfig::instance().addSortByModel(isAscending);
                break;
            case Config::YEAR:
                SorterConfig::instance().addSortByYear(isAscending);
                break;
            case Config::PRICE:
                SorterConfig::instance().addSortByPrice(isAscending);
                break;
        }
    }
    
    
    // настройка фильтрации
    // теоретически, можно отказать от конструкции switch и перейти на 2 вложенных
    // цикла. Методы установки и предикатов положить в map по ключу идентификатора поля
    // То же самое сделать и с предикатами, но по ключу (тип значения + операция сравнения),
    // т.к.все равно придется разводить из-за типа полей
    // Кода в итоге будет меньше, новые поля и предикаты будут подцепляться почти автоматом
    // (достаточно их положить в map)
    // Здесь пока еще функция обозрима, лучше оставить просто вариант - проще поддержка
    // кода даже для не слишком квалифицированного сотрудника
    Filter filter;
    for (auto const& it : config.filters())
    {
        switch (it.field())
        {
            case Config::BRAND:
            {
                std::string brand = boost::any_cast<std::string>(it.value());
                // пример использования предиката через лямбда-функцию
                filter.addBrandFilter([brand](boost::string_ref const& val) { return val == brand; });
                break;
            }
            case Config::YEAR:
            {
                using namespace std::placeholders;
                
                size_t value = boost::any_cast<size_t>(it.value());
                if (it.condition() == Config::LESS)
                    // пример использования предиката через функтор-структуру
                    filter.addYearFilter(Less<size_t>(value));
                else
                    // пример использования предиката через вызов функции с биндерами
                    filter.addYearFilter(std::bind(moreFunc<size_t>, _1, value));
                break;
            }
            case Config::PRICE:
            {
                float price = boost::any_cast<float>(it.value());
                switch (it.condition())
                {
                    case Config::EQUAL:
                        filter.addPriceFilter([price](float val) { return val == price; });
                        break;

                    case Config::MORE:
                        filter.addPriceFilter([price](float val) { return val > price; });
                        break;
                        
                    case Config::MORE_EQUAL:
                        filter.addPriceFilter([price](float val) { return val >= price; });
                        break;
                        
                    case Config::LESS:
                        filter.addPriceFilter([price](float val) { return val < price; });
                        break;
                        
                    case Config::LESS_EQUAL:
                        filter.addPriceFilter([price](float val) { return val <= price; });
                        break;
                        
                    case Config::NOT_EQUAL:
                        filter.addPriceFilter([price](float val) { return val != price; });
                        break;
                        
                }
                break;
            }
            case Config::MODEL:; // по модели не фильтруем
        }
    }

    
    // заготовка на будущее: если в параметрах не задана сортировка, то быстрее
    // было бы сохранять фильтрованые записи в простом контейнере,
    // а если еще и фильтрация не задана - напрямую связать потоки
    //typedef deque<Car> Cars;
    
    // контейнер отфильтрованных записей, он и выполняет сортировку
    typedef std::multiset<Car, Sorter> Cars;
    Cars cars;
    
    try
    {
        // для сбора инвормации о затраченном времени
        PerfomanceCounter perfCounter(config.isShowCpu());
        
        // получение и обработка данных
        {
            std::ifstream iStream; // может быть придется читать из файла
            ScopeExit iStreamRestorer; // порядок важен (после iStream)
            std::istream* input; // 'переключатель' потоков данных
            
            if (!config.input().empty())
            {
                // задано использование файла данных
                iStream.open(config.input());
                if (iStream.fail())
                {
                    std::stringstream ss;
                    ss << "open " << config.input();
                    throw std::runtime_error(ss.str());
                }
                // можно было перенаправить буфер ввода и всегда читать из std::cin, но не потребовалось
                /*
                std::streambuf *cinBuf = std::cin.rdbuf(); // сохранили старый буфер ввода
                std::cin.rdbuf(iStream.rdbuf()); // перенаправление входного потока
                iStreamRestorer.onExitHandler = [cinBuf]()->void
                {
                    std::cin.rdbuf(cinBuf);
                };
                */
                
                // теперь даже это действие не обязательно, т.к. при выходе из скопа объект iStream
                // разрушится и закроет файл
                iStreamRestorer.onExitHandler = [&iStream]()->void
                {
                    iStream.close();
                };
                input = &iStream;
            } else
                input = &std::cin;

            
            perfCounter.check();
            perfCounter.startCpuTimer();
            
            // В процесс обработки передается поток исходных данных и итератор ввода
            // При необходимости можно легко переключать контейнеры и, как следствие,
            // алгоритмы сортировки
            filter.process(*input, std::insert_iterator<Cars>(cars, cars.begin()));
        }
        
        perfCounter.check();
        perfCounter.stopCpuTimer();
        std::string filterSortCpuReport = perfCounter.cpuPerfomanceResult();
        perfCounter.startCpuTimer();

        // вывод данных
        if (!config.output().empty())
        {
            freopen(config.output().c_str(), "w", stdout);
        }
        
        for (auto const& car : cars)
        {
            //std::cout << car << std::endl; - самый медленный вывод. Работа через stream_buf напрямую
            // большого выигрыша по скорости не дает. Вот то ли дело стареньки сишные вызовы
            // Примеры переопределенных операторов оставлю в файле car.cpp
            car.println(stdout);
        }
        
        perfCounter.startCpuTimer();
        perfCounter.check();
        
        if (config.isShowCpu())
        {
            using namespace boost::chrono;
            
            PerfomanceCounter::Points const& ps = perfCounter.points();
            
            // выводим в std::err т.к. std::cout может быть перегружен для вывода данных
            std::cerr
                << "filter/sort cpu: " << filterSortCpuReport
                << "   file out cpu: " << perfCounter.cpuPerfomanceResult();
            
            // т.к. точно знаем, сколько точек отсчета сделали, можем напрямую
            // обращаться к ним по порядковым индексам
            std::cerr
                << "filter/sort time: " << duration_cast<milliseconds>(ps[1] - ps[0])
                << ", write time: " << duration_cast<milliseconds>(ps[2] - ps[1])
                << ", full time: " << duration_cast<milliseconds>(ps[2] - ps[0])
                << std::endl;
        }
    }
    catch(std::exception const& e)
    {
        std::cerr << "ERROR " << e.what() << std::endl;
        return IO_ERROR;
    }

    return SUCCESS;
}

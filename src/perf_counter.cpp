//
//  perf_counter.cpp
//  Project
//
//  Created by Александр Щукин on 21.04.16.
//
//

#include "perf_counter.hpp"


PerfomanceCounter::PerfomanceCounter(bool enable) :
    _enable(enable)
{
    if (_enable)
        // если счетчики нужны, сразу выделим место под хранение
        _points.reserve(10);
    _cpuTimer.stop();
}

// запоминает время в данной точке
void PerfomanceCounter::check()
{
    if (_enable)
    {
        TimePoint tp(Clock::now());
        _points.push_back(tp);
    }
}

// доступ к сохраненным значениям точек отсчета
PerfomanceCounter::Points const& PerfomanceCounter::points() const
{
    return _points;
}

void PerfomanceCounter::startCpuTimer()
{
    if (_enable)
        _cpuTimer.start();
}

void PerfomanceCounter::stopCpuTimer()
{
    if (_enable)
        _cpuTimer.stop();
}

std::string PerfomanceCounter::cpuPerfomanceResult() const
{
    if (_enable)
        return _cpuTimer.format();
    return std::string{};
}

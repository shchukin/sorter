//
//  sorter.cpp
//  Project
//
//  Created by Александр Щукин on 19.04.16.
//
//

#include "sorter.hpp"


namespace
{
    
}


bool Sorter::operator()(Car const& l, Car const& r) const
{
    return SorterConfig::instance().less(l, r);
}


namespace
{
    template<typename T>
    bool nullCompare(boost::optional<T> const& l, boost::optional<T> const& r, SorterConfig::CompareResult& result)
    {
        // TODO уменьшить количество сравнений
        if (!l && !r)
            result = SorterConfig::EQUAL;
        else if (!l && r)
            result = SorterConfig::LESS;
        else if (l && !r)
            result = SorterConfig::MORE;
        else
            return false; // null не нашли, потребуется сравнение значений
        return true; // проверка успешна
    }
    
    template<typename T>
    SorterConfig::CompareResult valueCompare(T const& l, T const& r)
    {
        if (l < r)
            return SorterConfig::LESS;
        if (l > r)
            return SorterConfig::MORE;
        return SorterConfig::EQUAL;
    }
    
    template<>
    SorterConfig::CompareResult valueCompare(std::string const& l, std::string const& r)
    {
        // для строк два сравнения через операторы '>' и '<' слишном накладно
   
        /*  а вот это странно, switch работает гораздо медленнее, чем if
        switch (l.compare(r))
        {
            case -1: return SorterConfig::LESS;
            case 1:  return SorterConfig::MORE;
            default: return SorterConfig::EQUAL;
        }
        */
        int res = l.compare(r);
        if (res < 0)
            return SorterConfig::LESS;
        if (res > 0)
            return SorterConfig::MORE;
        return SorterConfig::EQUAL;
    }
    
    template<typename T>
    SorterConfig::CompareResult fullCompare(boost::optional<T> const& l, boost::optional<T> const& r)
    {
        SorterConfig::CompareResult result;
        if (nullCompare(l, r, result))
            return result;
        return valueCompare(*l, *r);
    }
    
    
    struct BrandAsc : public SorterConfig::Condition
    {
        virtual SorterConfig::CompareResult less(Car const& l, Car const& r) const override
        {
            return fullCompare(l.brand, r.brand);
        }
    };
    
    struct BrandDesc : public BrandAsc
    {
        virtual SorterConfig::CompareResult less(Car const& l, Car const& r) const override
        {
            return BrandAsc::less(r, l);
        }
    };
    
    struct ModelAsc : public SorterConfig::Condition
    {
        virtual SorterConfig::CompareResult less(Car const& l, Car const& r) const override
        {
            return fullCompare(l.model, r.model);
        }
    };
    
    struct ModelDesc : public ModelAsc
    {
        virtual SorterConfig::CompareResult less(Car const& l, Car const& r) const override
        {
            return ModelAsc::less(r, l);
        }
    };
    
    struct YearAsc : public SorterConfig::Condition
    {
        virtual SorterConfig::CompareResult less(Car const& l, Car const& r) const override
        {
            return fullCompare(l.year, r.year);
        }
    };
    
    struct YearDesc : public YearAsc
    {
        virtual SorterConfig::CompareResult less(Car const& l, Car const& r) const override
        {
            return YearAsc::less(r, l);
        }
    };
    
    struct PriceAsc : public SorterConfig::Condition
    {
        virtual SorterConfig::CompareResult less(Car const& l, Car const& r) const override
        {
            return fullCompare(l.price, r.price);
        }
    };
    
    struct PriceDesc : public PriceAsc
    {
        virtual SorterConfig::CompareResult less(Car const& l, Car const& r) const override
        {
            return PriceAsc::less(r, l);
        }
    };
}



SorterConfig& SorterConfig::instance()
{
    static SorterConfig instance;
    return instance;
}

bool SorterConfig::less(Car const& l, Car const& r) const
{
    for (auto const& cond : _conds)
    {
        switch (cond.less(l,r))
        {
            case LESS: return true;
            case MORE: return false;
            case EQUAL:; // продолжаем сортировку по следующему полю + уйдет предупреждение компилятора
        }
    }
    return false; // если все поля равны (или не заданы условия сотировки) - false, используем строгое сравнение
}

void SorterConfig::addSortByBrand(bool asc)
{
    _conds.push_back(asc ? new BrandAsc() : new BrandDesc());
}

void SorterConfig::addSortByModel(bool asc)
{
    _conds.push_back(asc ? new ModelAsc() : new ModelDesc());
}

void SorterConfig::addSortByYear(bool asc)
{
    _conds.push_back(asc ? new YearAsc() : new YearDesc());
}

void SorterConfig::addSortByPrice(bool asc)
{
    _conds.push_back(asc ? new PriceAsc() : new PriceDesc());
}



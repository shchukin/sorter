//
//  generator.cpp
//  sorter
//
//  Created by Александр Щукин on 18.04.16.
//
//

#include "generator.hpp"
#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <ostream>

using namespace std;

/**
 * создает файл с тестовыми данными
 * strm - выходной поток данных
 * recCount - количество записей для записи в файл
 */
void generate(ostream& strm, size_t recCount)
{
    namespace br = boost::random;
    
    br::random_device rnd;
    
    // запись в поток рандомной строки
    auto genString = [&rnd](ostream& strm)
    {
        string const chars( // алфавит для рандомной строки
          "abcdefghijklmnopqrstuvwxyz"
          "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
          "1234567890"
          "!@#$%^&*()"
          "`~-_=+[{]}\\|;:,<.>/?");
        
        if (br::uniform_int_distribution<>(0, 100)(rnd) < 5) // в 5% случаев null - значение
        {
            strm << "null";
            return;
        }
        
        br::uniform_int_distribution<> index_dist(0, chars.size() - 1);
        strm << '"';
        int wordCount = br::uniform_int_distribution<>(0, 7)(rnd); // количество слов в строке
        for (int w = 0; w < wordCount; ++w)
        {
            if (w > 0)
                strm << ' ';
            int len = br::uniform_int_distribution<>(2, 12)(rnd); // количество символов в слове
            for (int i = 0; i < len; ++i)
                strm << chars[index_dist(rnd)];
        }
        strm << '"';
    };
    
    // запись в поток строки, с информацией о "машине"
    auto genRec = [&rnd, &genString](ostream& strm)
    {
        size_t year = br::uniform_int_distribution<>(1910, 2016)(rnd);
        genString(strm);
        strm << ' ';
        genString(strm);
        
        if (year > 1920)
            strm << ' ' << year;
        else
            strm << " null";
        
        float price = static_cast<float>(br::uniform_int_distribution<>(100, 1000 * 100)(rnd)) / 100.0f;
        if (price > 10)
            strm << ' ' << price;
        else
            strm << " null";
        strm << endl;
    };
    
    
    for (size_t i = 0; i < recCount; ++i)
        genRec(strm);
}

//
//  vector.hpp
//  Project
//
//  Created by Александр Щукин on 21.04.16.
//
//

#ifndef vector_hpp
#define vector_hpp

#include <type_traits>

/**
 * Где, как не в тестовом задании проводить экперименты
 * Две структуры выше объявлены с константными полями, что автоматически запрещает
 * им использовать оператор присваивания.
 * С другой стороны константность позволяет обойтись без приватных переменных
 * и написания геттеров
 * class A {
 *    int _x;
 * public:
 *    int const& x() { return _x; }
 * };
 * Одна проблема, стандартный vector не умеет с такими структурами работать
 * Почему бы не реализовать свой вектор, работающий с простыми структурами,
 * но не обладающими семантикой присваивания
 *
 * P.S. is_trivially_copyable не помог, структура с boost::any, которая хранит
 * std::string не может быть использована с этой реализацией (пытается дважды
 * освободить память). Возможно поможет std::is_pod и не даст больно стрельнуть
 * себе в ногу
 */
template<typename T, bool Enable = std::is_trivially_copyable<T>::value>
class vector // динамический вектор простых типов без семантики присваивания
{
public:
    typedef T* iterator;
    typedef T const* const_iterator;
    
private:
    iterator _data; // блок памяти, в котором хранятся данные (значение итератора begin())
    iterator _end; // указатель на элемент, за последним (значение итератора end())
    size_t _size; // размер выделенной памяти по данные
    
public:
    // N - первоначально выделяемый размер памяти под N элементов (аналог std::vector::reserve(N))
    vector(int N) :
        _data(reinterpret_cast<T*>(new char[sizeof(T) * N])), _end(_data), _size(N * sizeof(T))
    {}
    
    ~vector()
    {
        delete[] _data;
    }
    
    bool empty() const
    {
        return _end == _data;
    }
    
    void push_back(T const& t)
    {
        if (reinterpret_cast<char*>(_data) + _size == reinterpret_cast<char*>(_end)) // в _size размер выделенной памяти в байтах
        {
            T* newData = reinterpret_cast<T*>(new char[_size + _size]); // удваиваем размер
            ::memcpy(newData, _data, _size); // перенос данных в новое место
            delete[] _data;
            _data = newData;
            _end = _data + _size; // восстанавливаем итератор
            _size += _size; // новый размер выделенной памяти
        }
        new(_end++) T(t); // вставляем элемент в конец списка
    }
    
    // т.к. за пределы класса Config отдаем ссылку на константный контейнер, нужны и константные итераторы
    const_iterator begin() const
    {
        return _data;
    }
    
    const_iterator end() const
    {
        return _end;
    }
    
    iterator begin()
    {
        return _data;
    }
    
    iterator end()
    {
        return _end;
    }
    
    // доступ к последнему элементу
    T const& back() const
    {
        return *(_end - 1);
    }
    
    // замещение элемента в векторе в позиции итератора
    void replace(iterator pos, T const& t)
    {
        new(pos) T(t);
    }
};


#endif /* vector_hpp */

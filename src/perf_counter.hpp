//
//  perf_counter.hpp
//  Project
//
//  Created by Александр Щукин on 21.04.16.
//
//

#ifndef perf_counter_hpp
#define perf_counter_hpp

#include <boost/timer/timer.hpp>
#include <boost/chrono.hpp>
#include <vector>


class PerfomanceCounter
{
private:
    // обычного таймера достаточно, хотя можно использовать high_resolution_clock
    typedef boost::chrono::system_clock Clock;
    typedef boost::chrono::time_point<Clock> TimePoint;
    
public:
    typedef std::vector<TimePoint> Points;
    
    // если передали false, то все вызовы будут работать в холостую не создавая нагрузки
    PerfomanceCounter(bool enable);
    
    // запоминает время в данной точке
    void check();
    
    // доступ к сохраненным значениям точек отсчета
    Points const& points() const;
    
    void startCpuTimer();
    void stopCpuTimer();
    std::string cpuPerfomanceResult() const;
    
private:
    // переменную _enable можно было заменить на изменяемую имплементацию (pimpl)
    // но это здесь как из пушки по воробьям
    bool _enable;
    Points _points; // массив сохраненных точек отсчета
    boost::timer::cpu_timer _cpuTimer;
};


#endif /* perf_counter_hpp */

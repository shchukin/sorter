//
//  config.hpp
//  Project
//
//  Created by Александр Щукин on 20.04.16.
//
//

#ifndef config_hpp
#define config_hpp

#include <vector>
#include <string>
#include <boost/any.hpp>
#include "vector.hpp"

class Config
{
public:
    enum Field
    {
        MODEL, BRAND, YEAR, PRICE
    };
    
    enum Condition
    {
        EQUAL, NOT_EQUAL, LESS, LESS_EQUAL, MORE, MORE_EQUAL
    };
    
    enum Order
    {
        ASC, DESC
    };
    
    
    class FilterItem
    {
    private:
        Field _field;
        Condition _condition;
        boost::any _value;
        
    public:
        FilterItem(Field field, Condition condition, boost::any&& value);
        
        Field field() const;
        Condition condition() const;
        boost::any const& value() const;
    };
    
    struct SortItem
    {
        // Вот такое определение std::vector не очень любит
        // Более подробные комментарии можно найти в vector.hpp
        Field const field;
        Order const order;
        
        SortItem(Field field);
        SortItem(Field field, Order order);
    };
    
    typedef std::vector<FilterItem> Filters; // со строками такая штука не прокатывает
    typedef vector<SortItem> Sorting; // см. комментарии в vector.hpp

    
    Config();
    
    bool isGenerateTestData() const;
    bool isShowCpu() const;
    std::string const& input() const;
    std::string const& output() const;
    
    Filters const& filters() const;
    
    // можно было бы объявить тип итераторов
    //typedef Filters::const_iterator FilterIter;
    // и возвращать итераторы методами filterBegin() и filterEnd(),
    // но это уже излишнее усложнение для класса параметров,
    // которое никак не тянет на демонстрацию знаний
    // Да и для поддержки программы в целом проще возвращать ссылку
    // на контейнер => пусть возвращается константная ссылка на
    // контейнер с данными
    Sorting const& sorting() const;
    
    bool parseArgs(int argc, const char* argv[]);
    
private:
    bool _isGenerateTestData;
    bool _isShowCpu;
    std::string _input;
    std::string _output;
    Filters _filters;
    Sorting _sorting;
    
    void parseSortOptions(std::vector<std::string> const& sortOptions);
    void parseFilterOptions(std::vector<std::string> const& filterOptions);
};



#endif /* config_hpp */

//
//  filter.cpp
//  sorter
//
//  Created by Александр Щукин on 18.04.16.
//
//

#include "filter.hpp"
#include <ostream>
#include <iostream> // TODO только для отладки
#include <string>
#include <boost/tokenizer.hpp>
#include <boost/utility/string_ref.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include "car.hpp"
#include <set>
#include <deque>


namespace
{
    struct TokenizerFunc
    {
        bool _isInQuotes;
        
        TokenizerFunc() : _isInQuotes(false) {}
        
        template <typename InputIterator, typename Token>
        bool operator()(InputIterator& next, InputIterator end, Token& tok)
        {
            if (next == end)
                return false;
            
            InputIterator tokBegin = next;
            InputIterator tokEnd = next;
            for (; next != end; ++next)
            {
                if (*next == '"')
                {
                    _isInQuotes = !_isInQuotes;
                    if (!_isInQuotes)
                    {
                        // кавычка закрылась
                        tokEnd = next + 1; // учитываем в строке закрывающуюся кавычку
                        next += 2; // пропустим кавычку и пробел за ней
                        break;
                    }
                }
                else if ((*next == ' ') && (!_isInQuotes))
                {
                    ++next;
                    break;
                }
                // если добрались до этого места, значит добавляем еще один символ к токену
                // => итератор конца токена смещается еще на 1
                tokEnd = next + 1;
            }
            tok = Token(tokBegin, std::distance(tokBegin, tokEnd));
            return true;
        }
        
        void reset()
        {
            _isInQuotes = false;
        }
    };
    

    
    template<typename T>
    T convertValueImpl(boost::string_ref const& val);

    // экономия в 2 сек (~ 1/4 всего времени) по сравнению с lexical_cast<>
    template<>
    float convertValueImpl<float>(boost::string_ref const& val)
    {
        return ::atof(val.data());
    }
    
    // экономия в 0.2 сек по сравнению с lexical_cast<>
    template<>
    size_t convertValueImpl<size_t>(boost::string_ref const& val)
    {
        return ::atoll(val.data());
    }
    
    template<>
    boost::string_ref convertValueImpl(boost::string_ref const& val)
    {
        return val.substr(1, val.length() - 2); // в строках не храним символ кавычки
    }
    
    template<typename T>
    boost::optional<T> convertValue(boost::string_ref const& val)
    {
        static boost::string_ref null("null");
        
        if (val == null)
            return boost::optional<T>();
        
        return boost::optional<T>(convertValueImpl<T>(val));
    }

} // namespace



namespace
{
    template<typename T, typename F>
    bool applyFilters(boost::string_ref const& val, boost::optional<T>& store, std::vector<F> const& filters)
    {
        store = convertValue<T>(val);
        
        if (filters.empty())
        {
            // фильтров нет, любое значение подходит
            return true;
        }
        else if (store) // для фильтров следуем правилу NULL != NULL
        {
            for( auto const& f : filters)
                if(!f(*store))
                    return false;
            // все ок, значение есть и удовлетворяет условиям фильтра
            return true;
        }
        
        // либо не инициализировано, либо не попало в фильтр
        return false;
    }
}

bool Filter::checkBrand(boost::string_ref const& val, boost::optional<boost::string_ref>& store) const
{
    return applyFilters(val, store, _brandFilters);
}

bool Filter::checkModel(boost::string_ref const& val, boost::optional<boost::string_ref>& store) const
{
    store = convertValue<boost::string_ref>(val); // по модели не фильтруем 
    return true;
}

bool Filter::checkYear(boost::string_ref const& val, boost::optional<size_t>& store) const
{
    return applyFilters(val, store, _yearFilters);
}

bool Filter::checkPrice(boost::string_ref const& val, boost::optional<float>& store) const
{
    return applyFilters(val, store, _priceFilters);
}

void Filter::addBrandFilter(BrandFilterFunc const& filter)
{
    _brandFilters.push_back(filter);
}

void Filter::addYearFilter(YearFilterFunc const& filter)
{
    _yearFilters.push_back(filter);
}

void Filter::addPriceFilter(PriceFilterFunc const& filter)
{
    _priceFilters.push_back(filter);
}



template<typename Container>
void Filter::process(std::istream& strm, std::insert_iterator<Container> inserter)
{
    // объединение парсера с фильтрацией данных дает прирост в производительности
    // если, разобрав первое поле становится понятно, что запись не удовлетворяет условиям фильтра
    // то и нет смысла разбирать остальные значения записи, можно сразу перейти к следующей
    
    std::vector<char> buf(128);
    typedef boost::tokenizer<TokenizerFunc, const char*, boost::string_ref> tokenizer;
    //std::string s;   <------------ первый, но медленный вариант
    int i = 0, j = 0;
    tokenizer tok(nullptr, nullptr);
    while (strm && !strm.eof())
    {
        //std::getline(strm, s);   <------------ первый, но медленный вариант
        strm.get(buf.data(), buf.size()); // так быстрее, но еще точно есть куда ускорять
        size_t len = strm.gcount();
        // возможно не смогли дочитать до конца строки из-за небольшого размера буфера. стоит проверить
        while (!strm.eof() && strm.peek() != '\n')
        {
            // размера буфера не хватило
            size_t size = buf.size();
            buf.resize(size * 2);
            // данные достаются как C-строки => в конец буфера дописывается \0
            // => при следующем чтении \0 надо затереть => доступный размер увеличивается на size + 1
            strm.get(buf.data() + size-1, size + 1);
            len += strm.gcount();
            // TODO тут напрашивается проверка на валидное состояние потока, но это оставим до production-версии
        }
        if (!strm.eof())
            // просто заполнили буфер очередной порцией данных
            strm.ignore(); // пропускаем \n
        
        //tok.assign(s.data(), s.data() + s.length());   <------------ первый, но медленный вариант
        tok.assign(buf.data(), buf.data() + len);
        
        boost::optional<boost::string_ref> brand;
        boost::optional<boost::string_ref> model;
        boost::optional<size_t> year;
        boost::optional<float> price;
        
        auto end = tok.end();
        auto it = tok.begin();
        if (it != end && checkBrand(*it, brand))
            if (++it != end && checkModel(*it, model))
                if (++it != end && checkYear(*it, year))
                    if (++it != end && checkPrice(*it, price))
                    {
                        // все ок, можно добавлять в список для сортировки
                        inserter = Car(brand, model, year, price);
                    }
    }
}


template void Filter::process(std::istream &strm, std::insert_iterator<std::deque<Car>>);
template void Filter::process(std::istream &strm, std::insert_iterator<std::multiset<Car, Sorter>>);


template<typename T>
bool moreFunc(T val, T barier)
{
    return val > barier;
};

// явно инстанцируем шаблон для 2х типов, других пока не предвидется
template bool moreFunc(size_t, size_t);
template bool moreFunc(float, float);

template<typename T>
Less<T>::Less(T const& barier) :
    _barier(barier)
{}

template<typename T>
bool Less<T>::operator()(T const& value) const
{
    return value < _barier;
}

template class Less<size_t>;
template class Less<float>;
//
//  filter.hpp
//  sorter
//
//  Created by Александр Щукин on 18.04.16.
//
//

#ifndef filter_hpp
#define filter_hpp

#include <iosfwd>
#include <boost/utility/string_ref.hpp>
#include <boost/optional.hpp>
#include <vector>
#include <iterator>
#include "sorter.hpp"


/**
 * 
 */
class Filter
{
public:
    typedef std::function<bool(boost::string_ref const&)> BrandFilterFunc;
    typedef std::function<bool(size_t)> YearFilterFunc;
    typedef std::function<bool(float)>  PriceFilterFunc;
    
private:
    std::vector<BrandFilterFunc> _brandFilters;
    std::vector<YearFilterFunc> _yearFilters;
    std::vector<PriceFilterFunc> _priceFilters;
    
    bool checkBrand(boost::string_ref const& val, boost::optional<boost::string_ref>& store) const;
    bool checkModel(boost::string_ref const& val, boost::optional<boost::string_ref>& store) const;
    bool checkYear(boost::string_ref const& val, boost::optional<size_t>& store) const;
    bool checkPrice(boost::string_ref const& val, boost::optional<float>& store) const;
    
public:
    void addBrandFilter(BrandFilterFunc const& filter);
    void addYearFilter(YearFilterFunc const& filter);
    void addPriceFilter(PriceFilterFunc const& filter);
    
    template<typename Container>
    void process(std::istream& strm, std::insert_iterator<Container> inserter);
};



template<typename T>
bool moreFunc(T val, T barier);

template<typename T>
class Less
{
private:
    T const _barier;
    
public:
    Less(T const& barier);
    bool operator()(T const& value) const;
};


#endif /* filter_hpp */

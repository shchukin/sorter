//
//  car.hpp
//  Project
//
//  Created by Александр Щукин on 19.04.16.
//
//

#ifndef car_hpp
#define car_hpp

#include <boost/optional.hpp>
#include <boost/utility/string_ref.hpp>
#include <ostream>


struct Car
{
    Car();
    Car(boost::optional<boost::string_ref> brand, boost::optional<boost::string_ref> model, boost::optional<size_t> year, boost::optional<float> price);
    
    boost::optional<std::string> brand;
    boost::optional<std::string> model;
    boost::optional<size_t> year;
    boost::optional<float> price;
    
    void println(FILE* out) const;
};


std::ostream& operator<<(std::ostream& strm, Car const& car);


/*
 Car(Car&& other); - имело бы смысл (конкретно в этой программе, судя по профилировщику), если бы
 строки хранили как boost::optional<boost::string_ref>,
 но для этого пришлобы бы держать весь файл в памяти
 Оставлю как вариант для оптимизации на будущее
*/

#endif /* car_hpp */

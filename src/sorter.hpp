//
//  sorter.hpp
//  Project
//
//  Created by Александр Щукин on 19.04.16.
//
//

#ifndef sorter_hpp
#define sorter_hpp

#include "car.hpp"
#include <boost/ptr_container/ptr_vector.hpp>

// структура необходима для инстанцирования контейнера, в котором происходит сортировка
struct Sorter
{
    bool operator()(Car const& l, Car const& r) const;
};



class SorterConfig
{
public:
    enum CompareResult
    {
        LESS = -1,
        EQUAL,
        MORE
    };
    
    struct Condition
    {
        virtual ~Condition() {}
        virtual CompareResult less(Car const&, Car const&) const = 0;
    };
    
private:
    boost::ptr_vector<Condition> _conds;
    
public:
    static SorterConfig& instance();
    
    bool less(Car const&, Car const&) const;
    
    // методы добавляют поля для сортировки
    // asc - при значении true - сортировка по возрастанию, иначе по убыванию
    void addSortByBrand(bool asc);
    void addSortByModel(bool asc);
    void addSortByYear(bool asc);
    void addSortByPrice(bool asc);
};



#endif /* sorter_hpp */

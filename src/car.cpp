//
//  car.cpp
//  Project
//
//  Created by Александр Щукин on 19.04.16.
//
//

#include "car.hpp"
#include <stdlib.h>
#include <cmath>
#include <iostream>


Car::Car()
{}

Car::Car(boost::optional<boost::string_ref> brand, boost::optional<boost::string_ref> model, boost::optional<size_t> year, boost::optional<float> price) :
    year(year),
    price(price)
{
    if (brand)
        this->brand = std::string(brand->data(), brand->length());
    if (model)
        this->model = std::string(model->data(), model->length());
}



namespace
{
/*
    самый тормозной способ вывода простых данных в файл
 
    template <typename T>
    void printOptValImpl(std::ostream& strm, T const& val)
    {
        strm << val;
    };
    
    template <>
    void printOptValImpl(std::ostream& strm, std::string const& val)
    {
        strm << '"' << val << '"'; // т.к. в строках кавычки не храним, надо их добавить при выводе
    };
    
    template <typename T>
    void printOptVal(std::ostream& strm, boost::optional<T> const& val)
    {
        if (val)
            printOptValImpl(strm, *val);
        else
            strm << "null";
    };
*/
    
    
/*
    вариант чуть быстрее, но тоже недостаточно хорошо
 
    template <typename T>
    void printOptValImpl(std::streambuf& rdbuf, T const& val);
    
    template <>
    void printOptValImpl(std::streambuf& rdbuf, std::string const& val)
    {
        rdbuf.sputc('"'); // т.к. в строках кавычки не храним, надо их добавить при выводе
        rdbuf.sputn(val.data(), val.length());
        rdbuf.sputc('"');
    };
    
    template <>
    void printOptValImpl(std::streambuf& rdbuf, size_t const& val)
    {
        char buf[64];
        int len = ::snprintf(buf, sizeof(buf), "%zu", val);
        rdbuf.sputn(buf, len);
    };
    
    template <>
    void printOptValImpl(std::streambuf& rdbuf, float const& val)
    {
        // цена машины в рублях => не больше 2 знаков после запятой (копейки)
        //
        // можно было сделать так
        //char buf[64];
        //int len = ::snprintf(buf, sizeof(buf), "%.2f", val);
        //
        // "%.2f" всегда дает 2 знака после запятой, даже при наличии финальных нулей
        // можно сделать красиво, об этом ниже
        
        char buf[64];
        int len = ::snprintf(buf, sizeof(buf), "%.2f, val);
        rdbuf.sputn(buf, len);
    };
    
    
    template <typename T>
    void printOptVal(std::streambuf& rdbuf, boost::optional<T> const& val)
    {
        if (val)
            printOptValImpl(rdbuf, *val);
        else
            rdbuf.sputn("null", 4);
    };
*/
    
    
    // общий шаблон метода вывода в файл
    template <typename T>
    void printOptValImpl(FILE* file, T const& val);
    
    // вариант для строки
    template <>
    void printOptValImpl(FILE* file, std::string const& val)
    {
        char const q = '"';
        ::fwrite(&q, 1, 1, file); // т.к. в строках кавычки не храним, надо их добавить при выводе
        ::fwrite(val.data(), val.length(), 1, file);
        ::fwrite(&q, 1, 1, file);
    };
    
    // для целого числа
    template <>
    void printOptValImpl(FILE* file, size_t const& val)
    {
        char buf[64];
        int len = ::snprintf(buf, sizeof(buf), "%zu", val);
        ::fwrite(buf, len, 1, file);
    };
    
    // для вещественного
    template <>
    void printOptValImpl(FILE* file, float const& val)
    {
        // цена машины в рублях => не больше 2 знаков после запятой (копейки)
        //
        // можно было сделать так
        //char buf[64];
        //int len = ::snprintf(buf, sizeof(buf), "%.2f", val);
        //
        // но "%.2f" всегда дает 2 знака после запятой, даже при наличии финальных нулей.
        // Можно сделать красиво:
        
        char buf[64]; // TODO 861.57
        //float mod;
        //int order = int(::modff(val, &mod) * 100.0f); // число копеек
        int order = int(val * 100.0f) % 100; // число копеек
        // если делится на 10 - 1 знак, если копеек 0 - ни одного, иначе 1
        const char* format = order % 10 ? "%.2f" : (!order ? "%.0f" : "%.1f");
        int len = ::snprintf(buf, sizeof(buf), format, val);
        ::fwrite(buf, len, 1, file);
    };
    
    // общий шаблон для всех полей класса Car, если значение не задано, надо выводить null
    template <typename T>
    void printOptVal(FILE* file, boost::optional<T> const& val)
    {
        if (val)
            printOptValImpl(file, *val);
        else
            ::fwrite("null", 4, 1, file);
    };
    
    
} // namespace


std::ostream& operator<<(std::ostream& strm, Car const& car)
{
    // классический вариант вывода
    /*
    printOptVal(strm, car.brand);
    strm << ' ';
    printOptVal(strm, car.model);
    strm << ' ';
    printOptVal(strm, car.year);
    strm << ' ';
    printOptVal(strm, car.price);
    */
    
    
    /*
    if (&strm != &std::cout)
    {
        // минимальная оптимизация, если подсунули какой-то странный поток вывода
        std::streambuf *rdbuf = strm.rdbuf();

        printOptVal(*rdbuf, car.brand);
        rdbuf->sputc(' ');
        printOptVal(*rdbuf, car.model);
        rdbuf->sputc(' ');
        printOptVal(*rdbuf, car.year);
        rdbuf->sputc(' ');
        printOptVal(*rdbuf, car.price);
    } else {
     // достаточная оптимизиция, но условие, по которому сюда переходим ненадежно
     // для безопасности лучше сделать отдельную функцию вывода через стандартные
     // файловые потоки в стиле C
     
     char const space = ' ';
     printOptVal(::stdout, car.brand);
     ::fwrite(&space, 1, 1, ::stdout);
     printOptVal(::stdout, car.model);
     ::fwrite(&space, 1, 1, ::stdout);
     printOptVal(::stdout, car.year);
     ::fwrite(&space, 1, 1, ::stdout);
     printOptVal(::stdout, car.price);
    }
    */
    return strm;
}


void Car::println(FILE* out) const
{
    char const space = ' ';
    printOptVal(out, brand);
    ::fwrite(&space, 1, 1, out);
    printOptVal(out, model);
    ::fwrite(&space, 1, 1, out);
    printOptVal(out, year);
    ::fwrite(&space, 1, 1, out);
    printOptVal(out, price);
    char const newLine = '\n';
    ::fwrite(&newLine, 1, 1, out);
}


